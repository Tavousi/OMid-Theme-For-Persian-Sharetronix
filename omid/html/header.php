﻿<!--[if IE]>
<meta http-equiv="refresh" content="1; url=<?= $C->SITE_URL ?>error_browser">
<![endif]-->

<?php
	
	$this->user->write_pageview();
	
	$hdr_search	= ($this->request[0]=='members' ? 'users' : ($this->request[0]=='groups' ? 'groups' : ($this->request[0]=='search' ? $D->tab : 'posts') ) );
	
	$this->load_langfile('inside/header.php');
	
?>

<style type="text/css">
h3, h4, h5, h6 	{clear: both;font-weight: normal;}
/*  */
input , textarea, select {
	-moz-border-radius: 2px;
	-webkit-border-radius: 2px;
	padding: 3px;
	font-weight: normal;
	vertical-align: middle;
	border: 2px solid #E4E4E4;
	background-color:#fff;
}
</style>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- Theme By : A.T Tell:00989381585940 -->
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml">
	<head>
		<title><?= htmlspecialchars($D->page_title) ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="Keywords" content="<?= $this->lang('hdr_keywords') ?>">
		<meta name="Description" content="<?= $this->lang('hdr_description') ?>">
		<link rel="alternate" type="application/rss+xml" title="RSS Feed" href="<?= $C->SITE_URL ?>/rss/all:posts" />
		<link href="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/css/inside.css" type="text/css" rel="stylesheet" />
		<link href="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/css/layouts.css" type="text/css" rel="stylesheet" />
	        <link href="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/css/inside.root.css" type="text/css" rel="stylesheet" />
 

		<script type="text/javascript" src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/js/inside.js"></script>
		<script type="text/javascript" src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/js/inside_autocomplete.js"></script>
		<script type="text/javascript" src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/js/inside_postform.js"></script>
        <SCRIPT type="text/javascript" src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/js/stick.js"></SCRIPT>
		<script type="text/javascript" src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/js/inside_posts.js"></script>
		<script type="text/javascript" src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/js/swfobject.js"></script>
		<script type="text/javascript" src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/js/jquery-1.4.2.min.js"></script>
		<script type="text/javascript" src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/js/smileys.js"></script>
		<script type="text/javascript" src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/js/unitpngfix.js"></script>
        <script src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/js/jquery.js" type="text/javascript"></script>
        <script src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/js/facebox.js" type="text/javascript"></script>
		<base href="<?= $C->SITE_URL ?>" />
		<script type="text/javascript"> var siteurl = "<?= $C->SITE_URL ?>"; </script>
		
		<?php if( isset($D->page_favicon) ) { ?>
		<link href="<?= $D->page_favicon ?>" type="image/x-icon" rel="shortcut icon" />
		<?php } elseif( $C->HDR_SHOW_FAVICON == 1 ) { ?>
		<link href="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/favicon.ico ?>" type="image/x-icon" rel="shortcut icon" />
		<?php } elseif( $C->HDR_SHOW_FAVICON == 2 ) { ?>
		<link href="<?= $C->IMG_URL.'attachments/'.$this->network->id.'/'.$C->HDR_CUSTOM_FAVICON ?>" type="image/x-icon" rel="shortcut icon" />
		<?php } ?>
		<?php if(isset($D->rss_feeds)) { foreach($D->rss_feeds as &$f) { ?>
		<link rel="alternate" type="application/atom+xml" title="<?= $f[1] ?>" href="<?= $f[0] ?>" />
		<?php }} ?>
		<?php if( $this->user->is_logged && $this->user->info->js_animations == "0" ) { ?>
		<script type="text/javascript"> disable_animations = true; </script>
		<?php } ?>
		<?php if( $this->user->is_logged && $this->user->sess['total_pageviews'] == 1 ) { ?>
		<script type="text/javascript"> pf_autoopen = true; </script>
		<?php } ?>
		<?php if( isset($C->FACEBOOK_API_KEY) && !empty($C->FACEBOOK_API_KEY) ) { ?>
		<script src="http://static.ak.connect.facebook.com/js/api_lib/v0.4/FeatureLoader.js.php/en_US" type="text/javascript"></script>
		<script type="text/javascript">
			FB_RequireFeatures(["Api","Connect"], function() {
				//FB.FBDebug.logLevel=1;
				FB.Facebook.init('<?= $C->FACEBOOK_API_KEY ?>', '<?= $C->SITE_URL ?>xd_receiver.htm');
				<?php if( !$this->user->is_logged && $this->request[0]!='signin' && $this->request[0]!='signup' ) { ?>
				function hrd_func_fbconnected() {
					api = FB.Facebook.apiClient;
					api.users_getInfo( [api.get_session().uid], ['name', 'username', 'about_me', 'birthday_date', 'pic_big', 'profile_url', 'website', 'proxied_email'], function(result) {
						var inurl	= "";
						if(result && result.length && result.length == 1) {
							var uinfo	= result[0];
							inurl	+= "fb_uid="+api.get_session().uid;
							for(var k in uinfo) {
								inurl	+= "&fb["+k+"]="+encodeURIComponent(uinfo[k]);
							}
						}
						window.location.href	= "<?= $C->SITE_URL ?>signin/js:fbconnect/?"+inurl;
					} );
				}
				FB.Connect.ifUserConnected( hrd_func_fbconnected, function(){try{func_fbnotconnected();}catch(e){};} );
				<?php } else { ?>
				FB.Connect.ifUserConnected( function(){try{func_fbconnected();}catch(e){};}, function(){try{func_fbnotconnected();}catch(e){};} );
				<?php } ?>
			});
		 $(document).ready(function(){
			$('#example-5').sticklr({
				showOn		: 'click',
				stickTo     : 'right'
			});
	    });
		  </script>
  
		<?php } ?>
		<?php if( $this->lang('global_html_direction') == 'rtl' ) { ?>
		<style type="text/css"> #site { direction:rtl; } </style>
		<?php } ?>
	</head>
	<body>

   <div id="sitehb">
   <DIV id="dheadline" style="float:center;">
			<DIV id="dheadline2">
				<DIV id="dheadnav"><DIV class="menu"  style="padding-top:0px;">
	<UL>
		<LI><A href="<?= $C->SITE_URL ?>" class="<?= $this->request[0]=='dashboard'||$this->request[0]=='home'?'onnav':'' ?>"><B>خانه</B></A></LI>
		<LI><A href="<?= $C->SITE_URL ?>members" class="<?= $this->request[0]=='members'?'onnav':'' ?>"><B>کاربران</B></A>
			<UL>
            <LI><A href="<?= $C->SITE_URL ?>members">همه‌ي کاربران</A></LI>
            <LI><A href="<?= $C->SITE_URL ?>members/tab:admins">مديران شبکه</A></LI>
            <?php if( $this->user->is_logged ) { ?>
			<LI><A href="<?= $C->SITE_URL ?>members/tab:ifollow">دنبال مي‌کنم</A></LI>
			<LI><A href="<?= $C->SITE_URL ?>members/tab:followers">دنبالم مي‌کنند</A></LI>
            <?php } ?>
			</UL>
		</LI>
		<LI><A href="<?= $C->SITE_URL ?>groups" class="<?= $this->request[0]=='groups'?'onnav':'' ?>"><B>گروه‌ها</B></A>
			<UL>
            <LI><A href="<?= $C->SITE_URL ?>groups/tab:all">همه‌ی گروه‌ها</A></LI>
            <?php if( $this->user->is_logged ) { ?>
			<LI><A href="<?= $C->SITE_URL ?>groups/tab:my">گروه‌های من</A></LI>
            <?php } ?>
			</UL>
		</LI>
				<LI><A href="<?= $C->SITE_URL ?>faq" class="<?= $this->request[0]=='faq'?'onnav':'' ?>"><B>راهنما</B></A>
			<UL>
			<LI><A href="<?= $C->SITE_URL ?>faq/show:2">پرسشهای متداول</A></LI>
			<LI><A href="<?= $C->SITE_URL ?>contacts">درباره / تماس</A></LI>
			<LI><A href="<?= $C->SITE_URL ?>advertise">تبلیغات</A></LI>
			</UL>
		</LI>
		<LI><A href="<?= $C->SITE_URL ?>search" class="<?= $this->request[0]=='search'?'onnav':'' ?>"><B>جستجو</B></A>
			<UL>
			<LI><A href="<?= $C->SITE_URL ?>search/tab:post">جستجوی ارسالها</A></LI>
			<LI><A href="<?= $C->SITE_URL ?>search/tab:groups">جستجوی گروه‌ها</A></LI>
			<LI><A href="<?= $C->SITE_URL ?>search/tab:users">جستجوی کاربران</A></LI>
			</UL>
		</LI>
	</UL>
</DIV></DIV>
<?php if( $this->user->is_logged ) { ?>
<A href="<?= $C->SITE_URL ?><?= $this->user->info->username ?>" style="height: 35px;">
<img src="<?= $C->IMG_URL ?>avatars/thumbs2/<?= $this->user->info->avatar ?>" title="<?= $this->user->info->fullname ?>" id="head-img-avat" class="<?= $this->request[0]=='user'?'onnav':'' ?>" /></A>
<?php } ?>
				<DIV id="smallinks">
                <?php if( $this->user->is_logged ) { ?>
				<A href="<?= $C->SITE_URL ?>settings" class="<?= $this->request[0]=='settings'?'onnav':'' ?>">تنظیمات</A>
				<A href="<?= $C->SITE_URL ?>signout">خروج</A>
                <?php } else { ?>
                <A href="<?= $C->SITE_URL ?>signup" class="<?= $this->request[0]=='signup'?'onnav':'' ?>">عضویت</A>
				<A href="<?= $C->SITE_URL ?>signin" class="<?= $this->request[0]=='signin'?'onnav':'' ?>">ورود</A>
                <?php } ?>
				</DIV>
                    
			<DIV id="dtopsearch">
            <?php if( $this->user->is_logged ) { ?>
			<span>دوستانِ خود را به <?= htmlspecialchars($C->OUTSIDE_SITE_TITLE) ?> دعوت کنید.<a href="<?= $C->SITE_URL ?>invite" style="color:#85C9FF" dir="rtl"> [لینک] </a></span>
            	<?php } else { ?>
                	<span>نظرات و انتقادات خود را برای ما ارسال کنید.<a href="<?= $C->SITE_URL ?>contacts" style="color:#85C9FF" dir="rtl"> [لینک] </a></span>
                <?php } ?>	
				</DIV>	
			</DIV>
		</DIV>
   
    <div class="headerbox"><div id="siteh">
<a href="<?= $C->SITE_URL ?>dashboard" title="<?= htmlspecialchars($C->SITE_TITLE) ?>"><img style="float:left" src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/logo.png" alt="<?= htmlspecialchars($C->SITE_TITLE) ?>" /></a>
    <div id="topsearch" style="float:right">
							<form style="background-color:#888;" name="search_form" method="post" action="<?= $C->SITE_URL ?>search">
									<input type="hidden" name="lookin" value="<?= $hdr_search ?>" />
									<div id="searchbtn"><input type="submit" value="" /></div>
									<div class="searchselect">
										<a id="search_drop_lnk" href="javascript:;" onFocus="this.blur();" onClick="try{msgbox_close();}catch(e){}; dropdiv_open('search_drop_menu1');"><?= $this->lang('hdr_search_'.$hdr_search) ?></a>
										<div id="search_drop_menu1" class="searchselectmenu" style="display:none;">
											<a href="javascript:;" onClick="hdr_search_settype('posts',this.innerHTML);dropdiv_close('search_drop_menu1');" onFocus="this.blur();"><?= $this->lang('hdr_search_posts') ?></a>
											<a href="javascript:;" onClick="hdr_search_settype('users',this.innerHTML);dropdiv_close('search_drop_menu1');" onFocus="this.blur();"><?= $this->lang('hdr_search_users') ?></a>
											<a href="javascript:;" onClick="hdr_search_settype('groups',this.innerHTML);dropdiv_close('search_drop_menu1');" onFocus="this.blur();" style="border-bottom:0px;"><?= $this->lang('hdr_search_groups') ?></a>
										</div>
									</div>
									<div id="searchinput"><input type="text" name="lookfor" value="<?= isset($D->search_string)?htmlspecialchars($D->search_string):'' ?>" rel="autocomplete" autocompleteoffset="-6,4" /></div>
								</form>
							</div>
                                             
                             <div style="width:469px;height:61px;margin-bottom:4px;margin-top:4px;float:right" id="nethdr3">
					<div style="width:469px;height:61px" id="nethdr4">
						<div align="center" style="width:469px;height:61px" id="netnav2" class="specialhomelink2">
<a href="<?= $C->SITE_URL ?>advertise"> 
					<img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/ads.gif" border="0" title="محل درج تبلیغ شما" /></a>
						</div>
					</div>
				</div>   
            
    </div>
    </div>
	<div id="site">
      <div id="site2">
		<div id="site3">
			<div id="wholesite">
				<div id="pagebody">
