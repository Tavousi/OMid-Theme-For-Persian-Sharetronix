﻿<?php
	
	$this->load_template('header.php');
	
?>
                
		<div id="profile">
			<div id="profile2">
				<div id="profile_left">
            
					<?php if($D->is_my_profile) { ?>
					<div id="profileavatar"><a href="<?= $C->SITE_URL ?>settings/avatar"><img src="<?= $C->IMG_URL.'avatars/'.$D->usr->avatar ?>" alt="" border="0" style="-moz-border-radius: 5px;-webkit-border-radius: 5px;" /></a><div id="usermenu">
  <?php if($D->tab == 'updates') { ?>
							<a href="javascript:;" onclick="postform_open();" class="um_ptg" onmouseover="userpage_top_tooltip(this.firstChild.innerHTML);" onmouseout="userpage_top_tooltip('');" title="<?= $this->lang('usr_toplnks_newpost') ?>"></a><?php } ?>
							<a href="<?= $C->SITE_URL ?>settings" class="um_edit" onfocus="this.blur();" onmouseover="userpage_top_tooltip(this.firstChild.innerHTML);" onmouseout="userpage_top_tooltip('');" title="<?= $this->lang('usr_toplnks_settings') ?>"></a></div>               
</div>               
					<?php } else { ?>
<div id="profileavatar"><img src="<?= $C->IMG_URL.'avatars/'.$D->usr->avatar ?>" style="-moz-border-radius: 5px;-webkit-border-radius: 5px;" />
  <?php if( $this->user->is_logged ) { ?><div id="usermenu">
  <?php if($D->tab == 'updates') { ?>

							<a href="javascript:;" onclick="postform_open(({username:'<?= $D->usr->username ?>'}));" class="um_pm" onmouseover="userpage_top_tooltip(this.firstChild.innerHTML);" onmouseout="userpage_top_tooltip('');" title="<?= $this->lang('usr_toplnks_private',array('#USERNAME#'=>$D->usr->username)) ?>"></a>
							<a href="javascript:;" onclick="postform_mention('<?= $D->usr->username ?>',true);" class="um_atuser" onfocus="this.blur();" onmouseover="userpage_top_tooltip(this.firstChild.innerHTML);" onmouseout="userpage_top_tooltip('');" title="<?= $this->lang('usr_toplnks_mention',array('#USERNAME#'=>$D->usr->fullname)) ?>"></a><?php } ?>
							<a href="javascript:;" id="usrpg_btn_follow" style="<?= $D->i_follow_him?'display:none':'' ?>" onclick="user_follow('<?= $D->usr->username ?>',this,'usrpg_btn_unfollow','<?= addslashes($this->lang('msg_follow_user_on',array('#USERNAME#'=>$D->usr->username))) ?>');" class="um_follow" onfocus="this.blur();" onmouseover="userpage_top_tooltip(this.firstChild.innerHTML);" onmouseout="userpage_top_tooltip('');" title="<?= $this->lang('usr_toplnks_follow',array('#USERNAME#'=>$D->usr->fullname)) ?>"></a>
							<a href="javascript:;" id="usrpg_btn_unfollow" style="<?= $D->i_follow_him?'':'display:none' ?>" onclick="user_unfollow('<?= $D->usr->username ?>',this,'usrpg_btn_follow','<?= addslashes($this->lang('user_unfollow_confirm',array('#USERNAME#'=>$D->usr->username))) ?>','<?= addslashes($this->lang('msg_follow_user_off',array('#USERNAME#'=>$D->usr->username))) ?>');" class="um_unfollow" onfocus="this.blur();" onmouseover="userpage_top_tooltip(this.firstChild.innerHTML);" onmouseout="userpage_top_tooltip('');" title="<?= $this->lang('usr_toplnks_unfollow',array('#USERNAME#'=>$D->usr->fullname)) ?>"></a>
</div>
<?php } ?>
</div>
<?php } ?>
<?php if( !empty($D->usr->about_me) ) { ?>
<div class="greygrad"><div class="greygrad2"><div class="greygrad3">
<div style="margin-left:2px;">
							<table cellspacing="2">
								<tr>
								<td>درباره من : <?= htmlspecialchars(str_cut($D->usr->about_me, 250)) ?></td>
								</tr>
							</table>
						</div>

</div></div></div>&nbsp;   
					<?php } ?>
					<div class="ttl" style="margin-bottom:3px;">
						<div class="ttl2">
							<h3><?= $this->lang('usr_left_cnt_ttl') ?></h3>
							<?php if($D->is_my_profile) { ?>
							<a href="<?= $C->SITE_URL ?>settings/contacts" class="ttlink"><?= $this->lang('usr_left_editlink') ?></a>
							<?php } elseif($D->tab != 'info') { ?>
							<a href="<?= userlink($D->usr->username) ?>/tab:info" class="ttlink"><?= $this->lang('usr_left_cnt_more') ?></a>
							<?php } ?>
						</div>
					</div>
					<table cellpadding="0" cellspacing="3">
<?php if( !empty($D->usr->fullname) ) { ?>
      
							<tr>
								<td class="contactparam"><img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/cicons_username.png" alt="" title="&#1606;&#1575;&#1605; &#1705;&#1575;&#1605;&#1604;" /></td>
								<td class="contactvalue"><?= $D->usr->fullname?></td>
							</tr>
<?php } ?>
 <tr>
							<td class="contactparam"><img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/star.png" alt="" title="<?= htmlspecialchars($D->usr->fullname) ?>" /></td>
							<td class="contactvalue">
                                              <div style="display:none;">
                   <strong>  پست ها:  <?php  echo $num_postme = $D->numpostthiss ; ?></strong><br>        
                   <strong>  دنبال شوندگان:<?php echo $num_floweer = $D->numflww;?></strong><br>
                   <strong>  دنبال کنندگان:<?php echo $num_flowedd = $D->numflwwdd;?></strong><br>
                    <strong> ديدگاه ها:<?php echo $num_comm = $D->numcomment;?></strong><br>
                    <strong> بازنشرها:<?php echo $num_reshh = $D->numresh;?></strong><br>
                    <?php 
                                        $numliketome = $num_likeof*0.7;
                    $emtyaz_mg = $num_mg*0.7;
                    $emtyaz_joing = $num_joing*0.5;
                    $emtyaz_liketo = $num_liketo*0.1;
                    $emtyaz_postme = $num_postme*1.5;
                    $emtyaz_floweer = $num_floweer*0.4;
                    $emtyaz_comm = $num_comm*1;
                    $emtyaz_resh = $num_reshh*0.6;?>
  
                 <strong><font color="blue">امتياز 
کل:<?echo $total_emtyaz = 
$numliketome+$emtyaz_mg+$emtyaz_joing+$emtyaz_liketo+$emtyaz_postme+$emtyaz_floweer+$emtyaz_comm+$emtyaz_resh."<br>
</font></strong>";?></div>
               
وضعيت:  <?    if(0 < 
$total_emtyaz && $total_emtyaz < 200){
                    echo "تازه کار";
                    }elseif(200 < $total_emtyaz && $total_emtyaz < 500){
                    echo "معمولي";
                    }elseif(500 < $total_emtyaz && $total_emtyaz < 900){
                    echo "نيمه فعال";
                    }elseif(900 < $total_emtyaz && $total_emtyaz < 2000){
                    echo "فعال";
                    }elseif(2000 < $total_emtyaz && $total_emtyaz < 5000){
                    echo "نيمه پيشرفته";
                    }elseif(5000 < $total_emtyaz && $total_emtyaz < 10000){
                    echo "پيشرفته";
                    }?>
                            </td>
						</tr>
<?php if( !empty($D->usr->sense) ) { ?>
							<tr>
								<td class="contactparam"><img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/dash.small.png" width="16" height="16" alt="" title="&#1581;&#1575;&#1604;&#1578; &#1575;&#1605;&#1585;&#1608;&#1586;"  /></td>
								<td class="contactvalue"><?= $D->usr->sense?></td>
							</tr>
<?php } ?>
<?php if( !empty($D->usr->birthdate ) ) { ?>
							<tr>
								<td class="contactparam"><img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/cicons_birth.png" alt="" title="&#1578;&#1575;&#1585;&#1740;&#1582; &#1578;&#1608;&#1604;&#1583;"  /></td>
								<td class="contactvalue"><?= $D->usr->birthdate ?></td>
							</tr>
<?php } ?>
<?php if( !empty($D->usr->gender ) || !empty($D->usr->marital ) ) { ?>
							<tr>
								<td class="contactparam"><img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/cicons_gender.gif" alt="" title="&#1580;&#1606;&#1587;&#1740;&#1578; - &#1608;&#1590;&#1593;&#1740;&#1578; &#1578;&#1575;&#1607;&#1604;" /></td>
								<td class="contactvalue"><?= $D->usr->gender ?> - <?= $D->usr->marital?></td>
							</tr>
<?php } ?>
<?php if( !empty($D->usr->religion) ) { ?>
							<tr>
								<td class="contactparam"><img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/cicons_marital.png" alt="" title="&#1583;&#1740;&#1606;" /></td>
								<td class="contactvalue"><?= $D->usr->religion?></td>
							</tr>
<?php } ?>
<?php if( !empty($D->usr->state) ) { ?>
							<tr>
								<td class="contactparam"><img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/cicons_location.png" alt="" title="&#1575;&#1587;&#1578;&#1575;&#1606; &#1605;&#1581;&#1604; &#1586;&#1606;&#1583;&#1711;&#1740;" /></td>
								<td class="contactvalue"><?= $D->usr->state?></td>
							</tr>
<?php } ?>

							<?php if( !empty($D->usr_website) ) { ?>
							<tr>
								<td class="contactparam"><img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/cicons_site.gif" alt="" title="&#1570;&#1583;&#1585;&#1587; &#1587;&#1575;&#1740;&#1578;" /></td>
								<td class="contactvalue"><a href="<?= htmlspecialchars($D->usr_website) ?>" title="<?= htmlspecialchars($D->usr_website) ?>" target="_blank"><?= htmlspecialchars(str_cut(preg_replace('/^(http(s)?|ftp)\:\/\/(www\.)?/','',$D->usr_website),25)) ?></a></td>
							</tr>
							<?php } ?>
							<?php if( !empty($D->usr_pemail) && $D->he_follows_me) { ?>
							<tr>
								<td class="contactparam"><img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/cicons_mail.gif" alt="" title="&#1575;&#1740;&#1605;&#1740;&#1604;"  /></td>
								<td class="contactvalue"><a href="mailto:<?= htmlspecialchars($D->usr_pemail) ?>" target="_blank"><?= htmlspecialchars(str_cut($D->usr_pemail,25)) ?></a></td>
							</tr>
							<?php } ?>
							<?php if( !empty($D->usr_wphone) ) { ?>
							<tr>
								<td class="contactparam"><img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/cicons_phone.gif" alt="" /></td>
								<td class="contactvalue"><?= htmlspecialchars(str_cut($D->usr_wphone,25)) ?></td>
							</tr>
							<?php } ?>
							<?php if( !empty($D->usr_pphone) ) { ?>
							<tr>
								<td class="contactparam"><img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/cicons_phone.gif" alt=""  /></td>
								<td class="contactvalue"><?= htmlspecialchars(str_cut($D->usr_pphone,25)) ?></td>
							</tr>
$db2->query('SELECT user_hash FROM users_theme WHERE user_id="'.$u->id.'" AND user_bg_enable="1" LIMIT 1');
	if($tmp = $db->fetch_object()) {
		$D->usr_hash	= stripslashes($tmp->user_hash);
	}
							<?php } ?>
						</table>
                                 
                    
<?php if( count($D->some_followers) > 0 && !($D->tab=='coleagues'&&$D->filter=='followers') ) { ?>
					<div class="ttl" style="margin-bottom:8px; margin-top:4px;">
						<div class="ttl2">
							<h3><?= $this->lang('usr_left_followers') ?></h3>
							<?php if( count($D->some_followers) > 8 ) { ?>
							<a href="<?= $C->SITE_URL ?><?= $D->usr->username ?>/tab:coleagues/filter:followers" class="ttlink"><?php if( !empty($D->usr->num_followers) ) { ?>
(<?= htmlspecialchars($D->usr->num_followers) ?> <?= $this->lang('grplist_numfollowers') ?>)<?php } ?></a>
							<? } ?>
						</div>
					</div>
						<div class="slimusergroup" style="margin-right:1px; margin-left:6px; margin-bottom:5px;">
						<?php $i=0; foreach($D->some_followers as $u) { ?>
						<a href="<?= $C->SITE_URL ?><?= $u->username ?>" class="slimuser" title="<?= htmlspecialchars($u->username) ?>"><img src="<?= $C->IMG_URL ?>avatars/thumbs3/<?= $u->avatar ?>" alt="" /></a>
						<?php if(++$i==25) { break; } } ?>
					</div>
					<?php } ?>
					<?php if( count($D->usr->tags)>0 ) { ?>
					<div class="ttl" style="margin-top:5px; margin-bottom:5px;">
						<div class="ttl2">
							<h3><?= $this->lang('usr_left_tgsubx_ttl') ?></h3>
							<?php if($D->is_my_profile) { ?>
							<a href="<?= $C->SITE_URL ?>settings/profile" class="ttlink"><?= $this->lang('usr_left_editlink') ?></a>
							<?php } ?>
						</div>
					</div>
					<div class="taglist">
						<?php foreach($D->usr->tags as $t) { ?>
						<a href="<?= $C->SITE_URL ?>search/usertag:<?= urlencode($t) ?>" title="<?= htmlspecialchars($t) ?>"><?= htmlspecialchars(str_cut($t, 20)) ?></a>
						<? } ?>
					</div>
					<?php } ?>
					
					<?php if( count($D->post_tags) > 0 ) { ?>
					<div class="ttl" style="margin-top:5px; margin-bottom:5px;"><div class="ttl2"><h3><?= $this->lang('usr_left_posttags') ?></h3></div></div>
					<div class="taglist">
						<?php foreach($D->post_tags as $tmp) { ?>
						<a href="<?= $C->SITE_URL ?>search/posttag:%23<?= $tmp ?>" title="#<?= htmlspecialchars($tmp) ?>"><small>#</small><?= htmlspecialchars(str_cut($tmp,25)) ?></a>
						<?php } ?>
					</div>
					<?php } ?>
				</div>
				<div id="profile_right">
                
<?php if( $this->user->is_logged ) { ?><?php } else { ?>
<DIV id="usr-prfttl2">
<B style="font-weight: normal;">در صورتی که تمایل دارید <?= empty($D->usr->fullname) ? htmlspecialchars($D->usr->username) : htmlspecialchars($D->usr->fullname) ?> را دنبال کنید باید ابتدا وارد سایت شوید!</B>
</DIV>                
<?php } ?>

<DIV id="usr-prfttl">
<B style="color:#7f7e7d;font-weight: normal;"><?= empty($D->usr->fullname) ? htmlspecialchars($D->usr->username) : htmlspecialchars($D->usr->fullname) ?></B>
<DIV style="float:left; padding-right: 25px;">
<A href="<?= userlink($D->usr->username) ?>" class="<?= $D->tab=='updates'?'onptab':'' ?>"> <?= $this->lang('usr_tab_updates') ?> </A> |<A href="<?= userlink($D->usr->username) ?>/tab:info" class="<?= $D->tab=='info'?'onptab':'' ?>"> <?= $this->lang('usr_tab_info') ?></A> | 
<A href="<?= userlink($D->usr->username) ?>/tab:coleagues" class="<?= $D->tab=='coleagues'?'onptab':'' ?>"> <?= $this->lang('usr_tab_coleagues') ?> </A> <?php if($D->num_groups>0) { ?>| 
<A href="<?= userlink($D->usr->username) ?>/tab:groups" class="<?= $D->tab=='groups'?'onptab':'' ?>"> <?= $this->lang('usr_tab_groups') ?> </A><?php } ?>		
<A href="<?= $C->SITE_URL ?>rss/username:<?= $D->usr->username ?>" target="_blank"> <IMG src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/rss.png" class="rss-icon" style="margin: -3px 10px -3px 4px;"></A>
</DIV>
</DIV>
<div id="slim_msgbox" style="display:none;">
<strong id="slim_msgbox_msg"></strong>
<a href="javascript:;" onclick="msgbox_close('slim_msgbox'); this.blur();" onfocus="this.blur();"><b><?= $this->lang('pf_msg_okbutton') ?></b></a>
				</div>
			
				<?php if( $D->tab == 'updates' ) { ?>
                
                 <?php if( $this->user->is_logged ) { ?>
				<div id="postform" style="display:none;">
					<form name="post_form" action="" method="post" onsubmit="return false;">
						<div id="pf_posting" style="display:none;">
							<img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/loading.gif" alt="" /><b><?= $this->lang('pf_msg_posting') ?></b>
						</div>
						<div id="pf_loading" style="display:none;">
							<img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/loading.gif" alt="" /><b><?= $this->lang('pf_msg_loading') ?></b>
						</div>
						<div id="pf_postedok" style="display:none;">
							<strong id="pf_postedok_msg"></strong>
							<a href="javascript:;" onclick="postform_topmsg_close();" onfocus="this.blur();"><b><?= $this->lang('pf_msg_okbutton') ?></b></a>
						</div>
						<div id="pf_postederror" style="display:none;">
							<strong id="pf_postederror_msg"></strong>
							<a href="javascript:;" onclick="postform_topmsg_close();" onfocus="this.blur();"><b><?= $this->lang('pf_msg_okbutton') ?></b></a>
						</div>
						<div id="pf_mainpart" style="display:none; position: relative;">
							<script type="text/javascript">
								pf_msg_max_length	= <?= $C->POST_MAX_SYMBOLS ?>;
								pf_close_confirm	= "<?= $this->lang('pf_confrm_close') ?>";
								pf_rmatch_confirm	= "<?= $this->lang('pf_confrm_rmat') ?>";
							</script>
							<div id="pfhdr">
								<div id="pfhdrleft">
									<b id="pf_title_newpost" style="font-weight: normal;"><?= $this->lang('pf_title_newmsg') ?></b>
									<b id="pf_title_edtpost" style="display:none;font-weight: normal;"><?= $this->lang('pf_title_edtmsg') ?></b>
									<div id="sharewith_user" class="pmuser" style="display:none;">
										<strong><?= $this->lang('pf_title_newmsg_usr') ?></strong> <input type="text" name="username" value="" rel="autocomplete" autocompleteoffset="0,3" autocompleteafter="d.post_form.message.focus(); postform_sharewith_user(d.post_form.username.value);" onblur="postform_bgcheck_username();" />
										<a href="javascript:;" onclick="dropdiv_open('updateoptions',-2);" onfocus="this.blur();"></a>
									</div>
									<div id="sharewith_group" class="pmuser" style="display:none;">
										<strong><?= $this->lang('pf_title_newmsg_grp') ?></strong> <input type="text" name="groupname" value="" rel="autocomplete" autocompleteoffset="0,3" autocompleteafter="d.post_form.message.focus(); postform_sharewith_group(d.post_form.groupname.value);" onblur="postform_bgcheck_groupname();" />
										<a href="javascript:;" onclick="dropdiv_open('updateoptions',-2);" onfocus="this.blur();"></a>
									</div>
									<div id="sharewith" onclick="dropdiv_open('updateoptions',-2);">
										<a href="javascript:;" id="selectedupdateoption" onfocus="this.blur();"><span defaultvalue="<?= $this->lang('os_pf_title_newmsg_all') ?>"></span><b></b></a>
										<div id="updateoptions" style="display:none;">
											<a href="javascript:;" onclick="postform_sharewith_all('<?= $this->lang('os_pf_title_newmsg_all') ?>');"><?= $this->lang('os_pf_title_newmsg_all') ?></a>
											<?php if( $this->request[0]=='user' && $this->params->user && $this->params->user!=$this->user->id && $tmp=$this->network->get_user_by_id($this->params->user) ) { ?>
											<a href="javascript:;" onclick="postform_sharewith_user('<?= htmlspecialchars($tmp->username) ?>');" onfocus="this.blur();" title="<?= htmlspecialchars($u->fullname) ?>"><?= htmlspecialchars(str_cut($tmp->username, 30)) ?></a>
											<?php } ?>
											<?php foreach($this->user->get_top_groups(10) as $g) { ?>
											<a href="javascript:;" onclick="postform_sharewith_group('<?= htmlspecialchars($g->title) ?>');" onfocus="this.blur();" title="<?= htmlspecialchars($g->title) ?>"><?= htmlspecialchars(str_cut($g->title, 30)) ?></a>
											<?php } ?>
											
											<a href="javascript:;" onclick="postform_sharewith_findgroup();"><?= $this->lang('pf_title_newmsg_mngrp') ?></a>
											
											<a href="javascript:;" onclick="postform_sharewith_finduser();" style="border-bottom:0px;"><?= $this->lang('pf_title_newmsg_mnusr') ?></a>
										</div>
									</div>
								</div>
								<div id="pfhdrright">
									<a href="javascript:;" onclick="postform_close_withconfirm();" onfocus="this.blur();"></a>
									<small><?= $this->lang('pf_cnt_symbols_bfr') ?><span id="pf_chars_counter"><?= $C->POST_MAX_SYMBOLS ?></span><?= $this->lang('pf_cnt_symbols_aftr') ?></small>
								</div>
							</div>
							<textarea style="width:100%" name="message" tabindex="1"></textarea>
							<div id="pfattach">
									<? if( $C->ATTACH_LINK_DISABLED==1 ) { echo '<div style="display:none;">'; }  ?>
								<a href="javascript:;" class="attachbtn" onclick="postform_attachbox_open('link', 96); this.blur();" id="attachbtn_link" tabindex="3"><b><?= $this->lang('pf_attachtab_link') ?></b></a>
								<? if( $C->ATTACH_LINK_DISABLED==1 ) { echo '</div>'; }  ?>
								<div id="attachok_link" class="attachok" style="display:none;"><span><b><?= $this->lang('pf_attached_link') ?></b> <em id="attachok_link_txt"></em> <a href="javascript:;" class="removeattachment" onclick="postform_attach_remove('link');" onfocus="this.blur();"></a></span></div>
								<? if( $C->ATTACH_IMAGE_DISABLED==1 ) { echo '<div style="display:none;">'; }  ?>
								<a href="javascript:;" class="attachbtn" onclick="postform_attachbox_open('image', 131); this.blur();" id="attachbtn_image" tabindex="3"><b><?= $this->lang('pf_attachtab_image') ?></b></a>
								<? if( $C->ATTACH_IMAGE_DISABLED==1 ) { echo '</div>'; }  ?>
								<div id="attachok_image" class="attachok" style="display:none;"><span><b><?= $this->lang('pf_attached_image') ?></b> <em id="attachok_image_txt"></em> <a href="javascript:;" class="removeattachment" onclick="postform_attach_remove('image');" onfocus="this.blur();"></a></span></div>
								<? if( $C->ATTACH_VIDEO_DISABLED==1 ) { echo '<div style="display:none;">'; }  ?>
								<a href="javascript:;" class="attachbtn" onclick="postform_attachbox_open('videoembed', 96); this.blur();" id="attachbtn_videoembed" tabindex="3"><b><?= $this->lang('pf_attachtab_videmb') ?></b></a>
								<? if( $C->ATTACH_VIDEO_DISABLED==1 ) { echo '</div>'; }  ?>
								<div id="attachok_videoembed" class="attachok" style="display:none;"><span><b><?= $this->lang('pf_attached_videmb') ?></b> <em id="attachok_videoembed_txt"></em> <a href="javascript:;" class="removeattachment" onclick="postform_attach_remove('videoembed');" onfocus="this.blur();"></a></span></div>
								<? if( $C->ATTACH_FILE_DISABLED==1 ) { echo '<div style="display:none;">'; }  ?>
								<a href="javascript:;" class="attachbtn" onclick="postform_attachbox_open('file', 96); this.blur();" id="attachbtn_file" tabindex="3"><b><?= $this->lang('pf_attachtab_file') ?></b></a>
								<? if( $C->ATTACH_FILE_DISABLED==1 ) { echo '</div>'; }  ?>
								<div id="attachok_file" class="attachok" style="display:none;"><span><b><?= $this->lang('pf_attached_file') ?></b> <em id="attachok_file_txt"></em> <a href="javascript:;" class="removeattachment" onclick="postform_attach_remove('file');" onfocus="this.blur();"></a></span></div>
<a href="javascript:;" onclick="postform_submit();" tabindex="2"><b id="postbtn_newpost"></b><b id="postbtn_edtpost" style="display:none;"></b></a>
  <a title="&#1575;&#1585;&#1587;&#1575;&#1604; &#1662;&#1587;&#1578;" href="javascript:;" onclick="postform_submit();">
<button style="width:60px;" class="clean-orng">&#1575;&#1585;&#1587;&#1575;&#1604;</button></a><a href="javascript:;" href="javascript: void(0)" onclick="window.open('<?= $C->SITE_URL ?>smileys', 'windowname2', 'width=540, \height=400, \directories=no, \location=no, \menubar=no, \resizable=no, \scrollbars=yes, \status=no, \toolbar=no'); return false;">
<button style="width:30px;height:30px;" class="cupid-green">:)</button></a>                                                               
							</div>
						</div>
						<div id="attachbox" style="display:none;">
							<div id="attachboxhdr"></div>
							<div id="attachboxcontent">
								<div id="attachboxcontent_link" style="display:none;">
									<a href="javascript:;" class="closeattachbox" onclick="postform_attachbox_close();" onfocus="this.blur();"></a>
									<div class="attachform">
										<small id="attachboxtitle_link" defaultvalue="<?= $this->lang('pf_attachbx_ttl_link') ?>"></small>
										<input type="text" name="atch_link" value="" style="width:450px;" onpaste="postform_attach_pastelink(event,this,postform_attach_submit);" onkeyup="postform_attach_pastelink(event,this,postform_attach_submit);" />
									</div>
									<div id="attachboxcontent_link_ftr" class="submitattachment">
										<img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/loading.gif" alt="" style="margin-bottom:2px;" />
										<a href="javascript:;" class="submitattachmentbtn" onclick="postform_attach_submit();" onfocus="this.blur();"><b><?= $this->lang('pf_attachbtn_link') ?></b></a>
										<div class="orcancel"><?= $this->lang('pf_attachbtn_or') ?> <a href="javascript:;" onclick="postform_attachbox_close();" onfocus="this.blur();"><?= $this->lang('pf_attachbtn_orclose') ?></a></div>
									</div>
								</div>
								<div id="attachboxcontent_image" style="display:none;">
									<div class="litetabs">
										<a href="javascript:;" class="closeattachbox" onclick="postform_attachbox_close();" onfocus="this.blur();"></a>
										<a href="javascript:;" onclick="postform_attachimage_tab('upl');" id="attachform_img_upl_btn" class="onlitetab" onfocus="this.blur();"><b><?= $this->lang('pf_attachimg_tabupl') ?></b></a>
										<a href="javascript:;" onclick="postform_attachimage_tab('url');" id="attachform_img_url_btn" class="" onfocus="this.blur();"><b><?= $this->lang('pf_attachimg_taburl') ?></b></a>
									</div>
									<div class="attachform">
										<div id="attachform_img_upl_div">
											<small id="attachboxtitle_image_upl" defaultvalue="<?= $this->lang('pf_attachbx_ttl_imupl') ?>"></small>
											<input type="file" name="atch_image_upl" value="" size="50" />
										</div>
										<div id="attachform_img_url_div" style="display:none;">
											<small id="attachboxtitle_image_url" defaultvalue="<?= $this->lang('pf_attachbx_ttl_imurl') ?>"></small>
											<input type="text" name="atch_image_url" value="" style="width:450px;" onpaste="postform_attach_pastelink(event,this,postform_attach_submit);" onkeyup="postform_attach_pastelink(event,this,postform_attach_submit);" />
										</div>
									</div>
									<div id="attachboxcontent_image_ftr" class="submitattachment">
										<img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/loading.gif" alt="" style="margin-bottom:2px;" />
										<a href="javascript:;" class="submitattachmentbtn" onclick="postform_attach_submit();" onfocus="this.blur();"><b><?= $this->lang('pf_attachbtn_image') ?></b></a>
										<div class="orcancel"><?= $this->lang('pf_attachbtn_or') ?> <a href="javascript:;" onclick="postform_attachbox_close();" onfocus="this.blur();"><?= $this->lang('pf_attachbtn_orclose') ?></a></div>
									</div>
								</div>
								<div id="attachboxcontent_videoembed" style="display:none;">
									<a href="javascript:;" class="closeattachbox" onclick="postform_attachbox_close();" onfocus="this.blur();"></a>
									<div class="attachform">
										<small id="attachboxtitle_videoembed" defaultvalue="<?= $this->lang('pf_attachbx_ttl_videm') ?>"></small>
										<input type="text" name="atch_videoembed" value="" style="width:450px;" onpaste="postform_attach_pastelink(event,this,postform_attach_submit);" onkeyup="postform_attach_pastelink(event,this,postform_attach_submit);" />
									</div>
									<div id="attachboxcontent_videoembed_ftr" class="submitattachment">
										<img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/loading.gif" alt="" style="margin-bottom:2px;" />
										<a href="javascript:;" class="submitattachmentbtn" onclick="postform_attach_submit();" onfocus="this.blur();"><b><?= $this->lang('pf_attachbtn_videmb') ?></b></a>
										<div class="orcancel"><?= $this->lang('pf_attachbtn_or') ?> <a href="javascript:;" onclick="postform_attachbox_close();" onfocus="this.blur();"><?= $this->lang('pf_attachbtn_orclose') ?></a></div>
									</div>
								</div>
								<div id="attachboxcontent_file" style="display:none;">
									<a href="javascript:;" class="closeattachbox" onclick="postform_attachbox_close();" onfocus="this.blur();"></a>
									<div class="attachform">
										<small id="attachboxtitle_file" defaultvalue="<?= $this->lang('pf_attachbx_ttl_file') ?>"></small>
										<input type="file" name="atch_file" value="" size="50" />
									</div>
									<div id="attachboxcontent_file_ftr" class="submitattachment">
										<img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/loading.gif" alt="" style="margin-bottom:2px;" />
										<a href="javascript:;" class="submitattachmentbtn" onclick="postform_attach_submit();" onfocus="this.blur();"><b><?= $this->lang('pf_attachbtn_file') ?></b></a>
										<div class="orcancel"><?= $this->lang('pf_attachbtn_or') ?> <a href="javascript:;" onclick="postform_attachbox_close();" onfocus="this.blur();"><?= $this->lang('pf_attachbtn_orclose') ?></a></div>
									</div>
								</div>
							</div>
							<div id="attachboxftr"></div>
						</div>
					</form>
				</div>
               	<?php } ?>
                
					<div class="htabs">
						<li><a href="<?= userlink($D->usr->username) ?>/tab:updates/filter:all" class="<?= $D->filter=='all'?'onhtab':'' ?>"><?= $D->filter1_title ?></a></li>
						<li><a href="<?= userlink($D->usr->username) ?>/tab:updates/filter:posts" class="<?= $D->filter=='posts'?'onhtab':'' ?>"><?= $D->filter2_title ?></a></li>
						<li><a href="<?= userlink($D->usr->username) ?>/tab:updates/filter:tweets" class="<?= $D->filter=='tweets'?'onhtab':'' ?>"><?= $D->filter3_title ?></a></li>
						<li><a href="<?= userlink($D->usr->username) ?>/tab:updates/filter:rss" class="<?= $D->filter=='rss'?'onhtab':'' ?>"><?= $D->filter4_title ?></a></li>
					</div>
					<?php if($this->param('msg')=='deletedpost') { ?>
					<?= okbox($this->lang('msg_post_deleted_ttl'), $this->lang('msg_post_deleted_txt'), TRUE, 'margin-bottom:6px;') ?>
					<?php } ?>
				<div id="userposts">
						<div id="posts_html">
							<?= $D->posts_html ?>
						</div>
					</div>
				<?php } elseif( $D->tab == 'coleagues' ) { ?>
					<div class="htabs">
						<li><a href="<?= userlink($D->usr->username) ?>/tab:coleagues/filter:ifollow" class="<?= $D->filter=='ifollow'?'onhtab':'' ?>" >دنبال شدگان (<?= $D->fnums['ifollow'] ?>)</a></li>
					<li>	<a href="<?= userlink($D->usr->username) ?>/tab:coleagues/filter:followers" class="<?= $D->filter=='followers'?'onhtab':'' ?>">دنبال کنندگان (<?= $D->fnums['followers'] ?>)</a></li>
					</div>
					<div id="grouplist">
						<?= $D->users_html ?>
					</div>
				<?php } elseif( $D->tab == 'groups' ) { ?>
					<div id="grouplist">
						<div class="ttl" style=" margin-bottom:6px;">
							<div class="ttl2">
								<h3><?= $D->groups_title ?></h3>
								<?php if( $D->num_results > 1 ) { ?>
								<div id="postfilter">
									<a href="javascript:;" onclick="dropdiv_open('postfilteroptions');" id="postfilterselected" onfocus="this.blur();"><span><?= $this->lang('groups_orderby_'.$D->orderby) ?></span></a>
									<div id="postfilteroptions" style="display:none;">
										<a href="<?= userlink($D->usr->username) ?>/tab:groups/orderby:name" style="float:none;"><?= $this->lang('groups_orderby_name') ?></a>
										<a href="<?= userlink($D->usr->username) ?>/tab:groups/orderby:date" style="float:none;"><?= $this->lang('groups_orderby_date') ?></a>
										<a href="<?= userlink($D->usr->username) ?>/tab:groups/orderby:users" style="float:none;"><?= $this->lang('groups_orderby_users') ?></a>
										<a href="<?= userlink($D->usr->username) ?>/tab:groups/orderby:posts" style="float:none; border-bottom:0px;"><?= $this->lang('groups_orderby_posts') ?></a>
									</div>
									<span><?= $this->lang('groups_orderby_ttl') ?></span>
								</div>
								<?php } ?>
							</div>
						</div>
						<?= $D->groups_html ?>
					</div>
				<?php } elseif( $D->tab == 'info' ) { ?>
					<div>
						<?php if( !empty($D->usr->about_me) ) { ?>
						<p style="display:block;"><?= $this->lang('usr_info_section_aboutme') ?> :</p>
						<div class="greygrad">
							<div class="greygrad2">
								<div class="greygrad3" style="color:black;">
									<?= htmlspecialchars($D->usr->about_me) ?>
								</div>
							</div>
						</div>
						<?php } ?>
						<div class="ttl"><div class="ttl2">
							<h3><?= $this->lang('usr_info_section_details') ?></h3>
							<?php if( $D->is_my_profile ) { ?>
							<a class="ttlink" href="<?= $C->SITE_URL ?>settings/profile"><?= $this->lang('usr_info_edit') ?></a>
							<?php } ?>
						</div></div>
						<div style="margin-left:4px;">
							<table cellspacing="4">
								<?php if( !empty($D->usr->location) ) { ?>
								<tr>
									<td class="detailsparam"><?= $this->lang('usr_info_aboutme_location') ?></td>
									<td class="detailsvalue"><?= htmlspecialchars($D->usr->location) ?></td>
								</tr>
								<?php } ?>
								<?php if( !empty($D->usr->gender) ) { ?>
								<tr>
									<td class="detailsparam"><?= $this->lang('usr_info_aboutme_gender') ?></td>
									<td class="detailsvalue"><?= $this->lang('usr_info_aboutme_gender_'.$D->usr->gender) ?></td>
								</tr>
								<?php } ?>
								<?php if( !empty($D->birthdate) ) { ?>
								<tr>
									<td class="detailsparam"><?= $this->lang('usr_info_aboutme_birthdate') ?></td>
									<td class="detailsvalue"><?= $D->birthdate ?></td>
								</tr>
								<?php } ?>
								<?php if( !empty($D->i->website) ) { ?>
								<tr>
									<td class="detailsparam"><?= $this->lang('usr_info_aboutme_website') ?></td>
									<td class="detailsvalue"><a href="<?= htmlspecialchars($D->i->website) ?>" target="_blank"><?= htmlspecialchars($D->i->website) ?></a></td>
								</tr>
								<?php } ?>
								<tr>
									<td class="detailsparam"><?= $this->lang('usr_info_aboutme_datereg') ?></td>
									<td class="detailsvalue"><?= $D->date_register ?></td>
								</tr>
								<?php if( !empty($D->date_lastlogin) ) { ?>
								<tr>
									<td class="detailsparam"><?= $this->lang('usr_info_aboutme_datelgn') ?></td>
									<td class="detailsvalue"><?= $D->date_lastlogin ?></td>
								</tr>
								<?php } ?>
							</table>
						</div>
						<?php if( count($D->i->prs) > 0 ) { ?>
						<div class="ttl" style="margin-top:4px;"><div class="ttl2">
							<h3><?= $this->lang('usr_info_section_xtprofiles') ?></h3>
							<?php if( $D->is_my_profile ) { ?>
							<a class="ttlink" href="<?= $C->SITE_URL ?>settings/contacts"><?= $this->lang('usr_info_edit') ?></a>
							<?php } ?>
						</div></div>
						<div style="margin-left:4px;">
							<table cellspacing="4">
								<tr>
								<?php $i=0; foreach($D->i->prs as $k=>$v) { $i++; ?>
									<td><img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/ext_<?= $k ?>.gif" alt="<?= $this->lang('usr_info_'.$k) ?>" title="<?= $this->lang('usr_info_'.$k) ?>"></td>
									<td width="150"><a href="<?= htmlspecialchars($v[0]) ?>" target="_blank"><?= htmlspecialchars($v[1]) ?></a></td>
								<?php if($i%4==0 && count($D->i->prs)>$i) { ?>
								</tr>
								<tr>
								<?php } } ?>
								</tr>
							</table>
						</div>
						<?php } ?>
						<?php if( count($D->i->ims) > 0 ) { ?>
						<div class="ttl" style="margin-top:4px;"><div class="ttl2">
							<h3><?= $this->lang('usr_info_section_messengers') ?></h3>
							<?php if( $D->is_my_profile ) { ?>
							<a class="ttlink" href="<?= $C->SITE_URL ?>settings/contacts"><?= $this->lang('usr_info_edit') ?></a>
							<?php } ?>
						</div></div>
						<div style="margin-left:4px;">
							<table cellspacing="4">
								<tr>
								<?php $i=0; foreach($D->i->ims as $k=>$v) { $i++; ?>
									<td><img src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/<?= $k ?>.gif" alt="<?= $this->lang('usr_info_'.$k) ?>" title="<?= $this->lang('usr_info_'.$k) ?>" /></td>
									<td width="170"><?= htmlspecialchars($v) ?></td>
								<?php if($i%4==0 && count($D->i->ims)>$i) { ?>
								</tr>
								<tr>
								<?php } } ?>
								</tr>
							</table>
						</div>
						<?php } ?>
					</div>
				<?php } ?>
				</div>
				<div class="klear"></div>
			</div>
		</div>
<?php
	
	$this->load_template('footer.php');
	
?>