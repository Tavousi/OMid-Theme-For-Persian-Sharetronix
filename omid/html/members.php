<?php
	
	$this->load_template('header.php');
	
?>


					<div id="page_browse_mg">
						<div id="page_browse_mg_left">
							<h2><?= $D->leftcol_title ?></h2>
							<div class="greygrad">
								<div class="greygrad2">
									<div class="greygrad3">
										<?= $D->leftcol_text ?>
									</div>
								</div>
							</div>
							<?php if( $this->user->is_logged ) { ?>
							<div class="greygrad">
								<div class="greygrad2">
									<div class="greygrad3">
										<?= $this->lang('os_members_left_invite_text', array('#SITE_TITLE#'=>$C->OUTSIDE_SITE_TITLE)) ?>
										<div class="klear"></div>
										<center><p><a href="<?= $C->SITE_URL ?>invite"><button class="clean-gray"><?= $this->lang('os_members_left_invite_button') ?></button></a></p></center>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
						<div id="page_browse_mg_right">
                        
                        <div id="slim_msgbox" style="display:none;width:708;">
<strong id="slim_msgbox_msg"></strong>
<a href="javascript:;" onclick="msgbox_close('slim_msgbox'); this.blur();" onfocus="this.blur();"><b><?= $this->lang('pf_msg_okbutton') ?></b></a>
				</div>

							<div class="htabs" style="margin-bottom:6px; margin-top:0px;">
								<li><a href="<?= $C->SITE_URL ?>members" class="<?= $D->tab=='all'?'onhtab':'' ?>"><?= $this->lang('members_tabs_all') ?> (<?= $D->tabnums['all'] ?>)</a></li>
								<?php if( $this->user->is_logged ) { ?>
								<li><a href="<?= $C->SITE_URL ?>members/tab:ifollow" class="<?= $D->tab=='ifollow'?'onhtab':'' ?>"><?= $this->lang('members_tabs_ifollow') ?> (<?= $D->tabnums['ifollow'] ?>)</a></li>
								<li><a href="<?= $C->SITE_URL ?>members/tab:followers" class="<?= $D->tab=='followers'?'onhtab':'' ?>"><?= $this->lang('members_tabs_followers') ?> (<?= $D->tabnums['followers'] ?>)</a></li>
								<?php } ?>
								<li><a href="<?= $C->SITE_URL ?>members/tab:admins" class="<?= $D->tab=='admins'?'onhtab':'' ?>"><?= $this->lang('members_tabs_admins') ?> (<?= $D->tabnums['admins'] ?>)</a></li>
							</div>
							<div id="grouplist">
								<?= $D->users_html ?>
							</div>
						</div>
					</div>
<?php
	
	$this->load_template('footer.php');
	
?>