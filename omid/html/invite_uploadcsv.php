<?php
	
	$this->load_template('header.php');
	
?>
		<div id="invcenter">
			<h2><?= $this->lang('invite_title') ?></h2>			
			<div class="htabs" style="margin-bottom:6px; margin-top:0px;">
				<li><a href="<?= $C->SITE_URL ?>invite"><?= $this->lang('os_invite_tab_colleagues') ?></a></li>
				<li><a href="<?= $C->SITE_URL ?>invite/parsemail"><?= $this->lang('os_invite_tab_parsemail') ?></a></li>
				<li><a href="<?= $C->SITE_URL ?>invite/uploadcsv" class="onhtab"><?= $this->lang('os_invite_tab_uploadcsv') ?></a></li>
				<li><a href="<?= $C->SITE_URL ?>invite/personalurl"><?= $this->lang('os_invite_tab_personalurl') ?></a></li>
				<li><a href="<?= $C->SITE_URL ?>invite/sentinvites"><?= $this->lang('os_invite_tab_sentinvites') ?></a></li>
			</div>
			<div class="invinfo">
				<?= $this->lang('os_invite_txt_uploadcsv', array('#SITE_TITLE#'=>$C->SITE_TITLE)) ?>
			</div>

			<?php if( $D->error ) { ?>
			<?= errorbox($this->lang('inv_uplfile_error'), $this->lang($D->errmsg), TRUE, 'margin-bottom:5px;') ?>
			<?php } ?>
			
			<div class="greygrad">
				<div class="greygrad2">
					<div class="greygrad3" style="padding-bottom:0px;">
						<form method="post" action="" enctype="multipart/form-data">
							<table id="setform" cellspacing="5">
								<tr>
									<td class="setparam"><?= $this->lang('inv_uplfile_finp') ?></td>
									<td><input type="file" class="setinp" name="uplfile" value="" /></td>
								</tr>
								<tr>
									<td></td>
									<td><button type="submit" class="cupid-green" style="width:60px;"><?= $this->lang('inv_uplfile_fbtn') ?></button></td>
								</tr>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
<?php
	
	$this->load_template('footer.php');
	
?>