<?php
	
	$this->load_template('header.php');
	
?>
		<div id="invcenter">
			<h2><?= $this->lang('invite_title') ?></h2>			
			<div class="htabs" style="margin-bottom:6px; margin-top:0px;">
				<li><a href="<?= $C->SITE_URL ?>invite"><?= $this->lang('os_invite_tab_colleagues') ?></a></li>
				<li><a href="<?= $C->SITE_URL ?>invite/parsemail"><?= $this->lang('os_invite_tab_parsemail') ?></a></li>
				<li><a href="<?= $C->SITE_URL ?>invite/uploadcsv"><?= $this->lang('os_invite_tab_uploadcsv') ?></a></li>
				<li><a href="<?= $C->SITE_URL ?>invite/personalurl" class="onhtab"><?= $this->lang('os_invite_tab_personalurl') ?></a></li>
				<li><a href="<?= $C->SITE_URL ?>invite/sentinvites"><?= $this->lang('os_invite_tab_sentinvites') ?></a></li>
			</div>
			<div class="invinfo">
				<?= $this->lang('os_invite_txt_personalurl', array('#SITE_TITLE#'=>$C->SITE_TITLE, '#OUTSIDE_SITE_TITLE#'=>$C->OUTSIDE_SITE_TITLE)) ?>
			</div>
			<script type="text/javascript">
				function select_text(obj) {
				}
			</script>
			<div class="greygrad">
				<div class="greygrad2">
					<div class="greygrad3" style="padding-bottom:0px;">
						<?= $this->lang('inv_plnk_yourlink') ?><br /><hr />
						<div id="invitelink" onmousedown="select_text(this);" style="float:left;"><?= $D->invitation_link ?></div>
					</div>
				</div>
			</div>
		</div>
<?php
	
	$this->load_template('footer.php');
	
?>