<?php
	$D->p = &$D->post;
?>

<style>
Body {
	direction:rtl;
}
</style>

		<link href="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/css/layouts.css" type="text/css" rel="stylesheet" />
	        <link href="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/css/inside.root.css" type="text/css" rel="stylesheet" />

		<script type="text/javascript" src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/js/inside.js"></script>
		<script type="text/javascript" src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/js/inside_autocomplete.js"></script>
		<script type="text/javascript" src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/js/inside_postform.js"></script>
        <SCRIPT type="text/javascript" src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/js/stick.js"></SCRIPT>
		<script type="text/javascript" src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/js/inside_posts.js"></script>
		<script type="text/javascript" src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/js/swfobject.js"></script>
		<script type="text/javascript" src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/js/jquery-1.4.2.min.js"></script>
		<script type="text/javascript" src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/js/smileys.js"></script>
		<script type="text/javascript" src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/js/unitpngfix.js"></script>
        <script src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/js/jquery.js" type="text/javascript"></script>
        <script src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/js/facebox.js" type="text/javascript"></script>
		<base href="<?= $C->SITE_URL ?>" />
		<script type="text/javascript"> var siteurl = "<?= $C->SITE_URL ?>"; </script>
		
		<?php if( isset($D->page_favicon) ) { ?>
		<link href="<?= $D->page_favicon ?>" type="image/x-icon" rel="shortcut icon" />
		<?php } elseif( $C->HDR_SHOW_FAVICON == 1 ) { ?>
		<link href="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/favicon.ico ?>" type="image/x-icon" rel="shortcut icon" />
		<?php } elseif( $C->HDR_SHOW_FAVICON == 2 ) { ?>
		<link href="<?= $C->IMG_URL.'attachments/'.$this->network->id.'/'.$C->HDR_CUSTOM_FAVICON ?>" type="image/x-icon" rel="shortcut icon" />
		<?php } ?>
		<?php if(isset($D->rss_feeds)) { foreach($D->rss_feeds as &$f) { ?>
		<link rel="alternate" type="application/atom+xml" title="<?= $f[1] ?>" href="<?= $f[0] ?>" />
		<?php }} ?>
		<?php if( $this->user->is_logged && $this->user->info->js_animations == "0" ) { ?>
		<script type="text/javascript"> disable_animations = true; </script>
		<?php } ?>
		<?php if( $this->user->is_logged && $this->user->sess['total_pageviews'] == 1 ) { ?>
		<script type="text/javascript"> pf_autoopen = true; </script>
		<?php } ?>
		<?php if( isset($C->FACEBOOK_API_KEY) && !empty($C->FACEBOOK_API_KEY) ) { ?>
		<script src="http://static.ak.connect.facebook.com/js/api_lib/v0.4/FeatureLoader.js.php/en_US" type="text/javascript"></script>
		<script type="text/javascript">
			FB_RequireFeatures(["Api","Connect"], function() {
				//FB.FBDebug.logLevel=1;
				FB.Facebook.init('<?= $C->FACEBOOK_API_KEY ?>', '<?= $C->SITE_URL ?>xd_receiver.htm');
				<?php if( !$this->user->is_logged && $this->request[0]!='signin' && $this->request[0]!='signup' ) { ?>
				function hrd_func_fbconnected() {
					api = FB.Facebook.apiClient;
					api.users_getInfo( [api.get_session().uid], ['name', 'username', 'about_me', 'birthday_date', 'pic_big', 'profile_url', 'website', 'proxied_email'], function(result) {
						var inurl	= "";
						if(result && result.length && result.length == 1) {
							var uinfo	= result[0];
							inurl	+= "fb_uid="+api.get_session().uid;
							for(var k in uinfo) {
								inurl	+= "&fb["+k+"]="+encodeURIComponent(uinfo[k]);
							}
						}
						window.location.href	= "<?= $C->SITE_URL ?>signin/js:fbconnect/?"+inurl;
					} );
				}
				FB.Connect.ifUserConnected( hrd_func_fbconnected, function(){try{func_fbnotconnected();}catch(e){};} );
				<?php } else { ?>
				FB.Connect.ifUserConnected( function(){try{func_fbconnected();}catch(e){};}, function(){try{func_fbnotconnected();}catch(e){};} );
				<?php } ?>
			});
		 $(document).ready(function(){
			$('#example-5').sticklr({
				showOn		: 'click',
				stickTo     : 'right'
			});
	    });
		  </script>


		<?php } ?>
		<?php if( $this->lang('global_html_direction') == 'rtl' ) { ?>
		<style type="text/css"> #site { direction:rtl; } </style>
		<?php } ?>

		<div id="viewpost">
			<div id="vposthdr">
				<div id="vposthdr2">


		
			<script type="text/javascript">
				postcomments_open_state["<?= $D->post->post_tmp_id ?>"]	= 1;
			</script>
			<a name="comments"></a>
			<div class="postcomments" id="postcomments_<?= $D->post->post_tmp_id ?>">
				<?php if( $D->post->post_commentsnum == 0 ) { ?>
					<?php if( $this->user->is_logged ) { ?>
					<div class="slimpostcommentshdr"><div class="slimpostcommentshdr2"></div></div>
					<div class="postcommentsftr">
						<div class="postcommentsftr2" style="padding-top:2px;">
	 						<div class="addpc_big">
																<div class="addpc_right">
									<textarea style="width:200px;" id="postcomments_<?= $D->post->post_tmp_id ?>_textarea" onkeyup="textarea_autoheight(this);"></textarea><br>
	<input class="ctrl-subs" style="width:100px;height:30px;" id="postcomments_<?= $D->post->post_tmp_id ?>_submitbtn" onclick="postcomments_submit('<?= $D->post->post_tmp_id ?>');" type="submit" value="<?= $this->lang('viewpost_comment_submit') ?>" />
                                
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
				<?php } else { ?>
				<div class="postcommentshdr">
					<div class="postcommentshdr2">
						
					</div>
				</div>

				</div>
				<div class="postcommentsftr">
					<div class="postcommentsftr2">
						<?php if( $this->user->is_logged ) { ?>
	 					<div class="addpc_big" id="postcomments_<?= $D->post->post_tmp_id ?>_bigform">
							
							<div class="addpc_right">
							
								<textarea style="width:200px;" id="postcomments_<?= $D->post->post_tmp_id ?>_textarea" onkeyup="textarea_autoheight(this);"></textarea><br>
								<input class="ctrl-subs" style="width:100px;height:30px;" id="postcomments_<?= $D->post->post_tmp_id ?>_submitbtn" onclick="postcomments_submit('<?= $D->post->post_tmp_id ?>');" type="submit" value="<?= $this->lang('viewpost_comment_submit') ?>" />

							</div>
						</div>
						<?php } ?>
					</div>
				</div>
				<?php } ?>
			</div>
			<div class="klear"></div>