<?php
	
	$this->load_template('header.php');
	
?>

<div id="slim_msgbox" style="display:none;width:708;">
<strong id="slim_msgbox_msg"></strong>
<a href="javascript:;" onclick="msgbox_close('slim_msgbox'); this.blur();" onfocus="this.blur();"><b><?= $this->lang('pf_msg_okbutton') ?></b></a>
				</div>


					<div id="invcenter">
						<h2>

							<div class="klear"></div>
						</h2>
						<?php if( $this->param('msg')=='deleted' ) { ?>
						<?= okbox($this->lang('groups_msgbox_deleted_ttl'), $this->lang('groups_msgbox_deleted_txt')) ?>
						<?php } ?>	
						<?php if( $this->user->is_logged ) { ?>
						<div class="htabs" style="margin-bottom:6px; margin-top:0px; overflow:visible;">
							<?php if( $D->tabnums['my'] == 0 ) { ?>
							<li><a href="<?= $C->SITE_URL ?>groups/tab:all" class="<?= $D->tab=='all'?'onhtab':'' ?>"><?= $this->lang('groups_page_tab_all') ?> (<?= $D->tabnums['all'] ?>)</a></li>
							<li><a href="<?= $C->SITE_URL ?>groups/tab:my" class="<?= $D->tab=='my'?'onhtab':'' ?>"><?= $this->lang('groups_page_tab_my') ?> (<?= $D->tabnums['my'] ?>)</a></li>
							<?php } else { ?>
							<li><a href="<?= $C->SITE_URL ?>groups/tab:my" class="<?= $D->tab=='my'?'onhtab':'' ?>"><?= $this->lang('groups_page_tab_my') ?> (<?= $D->tabnums['my'] ?>)</a></li>
							<li><a href="<?= $C->SITE_URL ?>groups/tab:all" class="<?= $D->tab=='all'?'onhtab':'' ?>"><?= $this->lang('groups_page_tab_all') ?> (<?= $D->tabnums['all'] ?>)</a></li>
                           
                           							<?php if( $D->num_results > 1 ) { ?>
							<div id="postfilter" style="float:left;">
								<a href="javascript:;" onclick="dropdiv_open('postfilteroptions');" id="postfilterselected" onfocus="this.blur();"><span><?= $this->lang('groups_orderby_'.$D->orderby) ?></span></a>
								<div id="postfilteroptions" style="display:none;">
									<a href="<?= $C->SITE_URL ?>groups/tab:<?= $D->tab ?>/orderby:name" style="float:none;"><?= $this->lang('groups_orderby_name') ?></a>
									<a href="<?= $C->SITE_URL ?>groups/tab:<?= $D->tab ?>/orderby:date" style="float:none;"><?= $this->lang('groups_orderby_date') ?></a>
									<a href="<?= $C->SITE_URL ?>groups/tab:<?= $D->tab ?>/orderby:users" style="float:none;"><?= $this->lang('groups_orderby_users') ?></a>
									<a href="<?= $C->SITE_URL ?>groups/tab:<?= $D->tab ?>/orderby:posts" style="float:none; border-bottom:0px;"><?= $this->lang('groups_orderby_posts') ?></a>
								</div>
								<span><?= $this->lang('groups_orderby_ttl') ?></span>
							
							<?php } ?>
						</div>
                           
                            <li style="float:left;"><a href="<?= $C->SITE_URL ?>groups/new"><?= $this->lang('groups_page_tab_add') ?></a></li>
                            <li style="float:left;"><a href="<?= $C->SITE_URL ?>search/tab:groups">جستجو در گروه‌ها</a></li>
							<?php } ?>
                           
 </div>
						<?php } else { ?>
						<div class="htabs" style="margin:0px; margin-bottom:6px; height:1px;">
                        </div>
						<?php } ?>
						<div id="grouplist" class="groupspage">
							<?= $D->groups_html ?>
						</div>
					</div>
<?php
	
	$this->load_template('footer.php');
	
?>