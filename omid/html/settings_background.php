<?php
	
	$this->load_template('header.php');
	
?>
<?php
    $index_img = $D->currw/$D->currh;
    if($D->currw>800){
        $view_img_w = 800;
        $view_img_h = $view_img_w / $index_img;
    }elseif($D->currw<200){
         $view_img_w = 200;
         $view_img_h = $view_img_w / $index_img;
    } else {
         $view_img_w = $D->currw;
         $view_img_h = $D->currh;
    }

?>
					<div id="settings">
				
							<?php $this->load_template('settings_leftmenu.php') ?>
					
					
							<?php if($D->error) { ?>
							<?= errorbox($this->lang('st_pbg_err'), $this->lang($D->errmsg)) ?>
							<?php } elseif($D->submit) { ?>
							<?= okbox($this->lang('st_pbg_ok'), $this->lang('st_pbg_okmsg')) ?>
							<?php } elseif(isset($D->msg) && $D->msg=='deleted') { ?>
							<?= okbox($this->lang('st_pbg_ok'), $this->lang('st_pbg_okdelmsg')) ?>
							<?php } ?>
							<div class="ttl">
                                                            <div class="ttl2">
								<h3><?= $this->lang('settings_pbg_ttl') ?></h3>
								<a class="ttlink" href="<?= $C->SITE_URL ?><?= $this->user->info->username ?>/tab:info"><?= $this->lang('settings_viewprofile_link') ?></a>
                                                            </div>
                                                        </div>
                                                        <form method="post" action="<?= $C->SITE_URL ?>settings/background/" enctype="multipart/form-data">
								<table id="setform" cellspacing="5">
									<tr>
										<td class="setparam" valign="top"><?= $this->lang('st_pbg_current') ?></td>
										<td><a href="javascript:;" onclick="flybox_open(<?= $view_img_w+34 ?>,<?= $view_img_h+129 ?>,'<?= $this->lang('st_pbg_current_bck_flybox') ?>','<?= htmlspecialchars('<img src="'.$C->IMG_URL.'pbg/'.$D->u->info->pbg.'" style="width:'.$view_img_w.'px;height:'.$view_img_h.'px;margin-top:5px;margin-left:5px;" alt="" />') ?>');"><img src="<?= $C->IMG_URL ?>pbg/<?= $D->u->info->pbg ?>" alt="" border="0" width="100" /></a></td>
                                                                                <td class="setparam" valign="top"><?= $this->lang('st_pbg_change_bck') ?></td>
										<td valign="top">
                                                                                    <input type="file" name="pbg" value="" class="setinp" /><br>
                                                                                    <span class="setparam" style="text-align:left; font-size:10px; padding:0px; padding-left:2px;"><?= $this->lang('st_pbg_change_info') ?></span><br><br>
                                                                                    <div style="color: #454545"><b>HINT</b>: You can choose a picture even from you computer, or from showed pictures bellow. In both cases, after making a choice click "<b>Upload picture</b>" button</div>
                                                                                </td>
                                                                        </tr>
									<tr>
										<td>&nbsp</td>
										<td nowrap colspan="2">
                                                                                    <?php if($D->u->info->pbg != "nobck.gif") { ?>
                                                                                    <a href="<?= $C->SITE_URL ?>settings/background/del:current" onclick="return confirm('<?= $this->lang('st_pbg_upload_delete_confirm') ?>');" onfocus="this.blur();"><?= $this->lang('st_pbg_upload_or_delete') ?></a>
                                                                                    <?php } ?>
										</td>
                                                                                <td><input type="submit" value="<?= $this->lang('st_pbg_uploadbtn') ?>" style="padding:4px; font-weight:bold;"/></td>
									</tr>
								</table>
                                                            <div style="margin-top:20px; margin-left: 20px">
                                                            <?php 
                                                                foreach ($D->back_picture as $key => $image){                                                                   
                                                                    echo "<div align=center style='float:left; width 110px; margin-right: 10px;'>";                                                                    
                                                                    echo '<input type=radio name="bck_picture" value="' . $image . '"><br><img src="/i/pbg/default/' . $image . '" width=100 height=100>';
                                                                    echo "</div>";
                                                                }
                                                                echo "<div style='clear:both'></div>";
                                                            ?>            
                                                            </div>
							</form>                                                  
						</div>
					
<?php
	
	$this->load_template('footer.php');
	
?>