﻿<?php
	
	$this->load_langfile('outside/footer.php');
	$this->load_langfile('inside/footer.php');
	
?>
				</div>
			</div>
			<div class="klear"></div>
<div id="footernl"><span style="color:#888;">
<a href="<?= $C->SITE_URL ?>"><?= $this->lang('hdr_nav_home') ?></a>&nbsp; |&nbsp;
<?php if( $this->user->is_logged ) { ?>
<a href="<?= $C->SITE_URL.$this->user->info->username ?>"><?= $this->lang('ftrlinks_sgn_profile') ?></a>&nbsp; |&nbsp;<?php } ?> 
<a href="<?= $C->SITE_URL ?>groups/tab:all"><?= $this->lang('ftrlinks_sgr_allgroups') ?></a>&nbsp; |&nbsp;
<a href="<?= $C->SITE_URL ?>members"><?= $this->lang('os_ftrlinks_sf_members', array('#SITE_TITLE#'=>$C->SITE_TITLE)) ?></a>&nbsp; |&nbsp;
<a href="<?= $C->SITE_URL ?>about">درباره ما</a>&nbsp; |&nbsp;
<a href="<?= $C->SITE_URL ?>advertise">تبلیغات</a>&nbsp; |&nbsp;
<a href="<?= $C->SITE_URL ?>faq"><?= $this->lang('os_ftrlinks_sa_faq') ?></a>&nbsp; |&nbsp;
<?php if( isset($C->TERMSPAGE_ENABLED,$C->TERMSPAGE_CONTENT) && $C->TERMSPAGE_ENABLED==1 && !empty($C->TERMSPAGE_CONTENT) ) { ?>
<a href="<?= $C->SITE_URL ?>terms"><?= $this->lang('os_ftrlinks_sa_terms') ?></a>&nbsp; |&nbsp;<?php } ?>
<a href="<?= $C->SITE_URL ?>contacts"><?= $this->lang('ftr_contacts') ?></a>
	
<?php
	require_once( $C->INCPATH.'helpers/func_languages.php' );
	$D->language		= $C->LANGUAGE;
	$D->menu_languages	= array();
	foreach(get_available_languages(FALSE) as $k=>$v) {
		$D->menu_languages[$k]	= $v->name;
	}
?>
<form method="post" action="<?= $C->SITE_URL ?>settings/system" style="float:left;">
<select name="language">
<?php foreach($D->menu_languages as $k=>$v) { ?>
<option value="<?= $k ?>"<?= $k==$D->language?' selected="selected"':'' ?>><?= htmlspecialchars($v) ?></option>
<?php } ?>
</select>
</form>

<br /><?= htmlspecialchars($C->OUTSIDE_SITE_TITLE) ?>
<?php if( $this->user->is_logged ) { ?>
<?php if( $this->user->info->is_network_admin ) { ?>
&nbsp; |&nbsp; <a href="<?= $C->SITE_URL ?>admin"><?= $this->lang('ftrlinks_sgn_admin') ?></a>
<?php } else { ?>
&nbsp; |&nbsp;  طراح قالب : A.T   
<?php } ?><?php } ?>
			
</div></div>
			
		<div id="flybox_container" style="display:none;">
			<div class="flyboxbackgr"></div>
			<div class="flybox" id="flybox_box">
				<div class="flyboxttl">
					<div class="flyboxttl_left"><b id="flybox_title"></b></div>
					<div class="flyboxttl_right"><a href="javascript:;" title="<?= $this->lang('post_atchbox_close') ?>" onfocus="this.blur();" onclick="flybox_close();"></a></div>
				</div>
				<div class="flyboxbody"><div class="flyboxbody2" id="flybox_main"></div></div>
				<div class="flyboxftr"><div class="flyboxftr2"></div></div>
			</div>
		</div>
		<?php if( isset($C->FACEBOOK_API_KEY) && !empty($C->FACEBOOK_API_KEY) ) { ?>
			<script type="text/javascript">
				try { FB.XFBML.Host.parseDomTree(); } catch(e) {}
			</script>
		<?php } ?>
		
		<?php
			// Important - do not remove this:
			$this->load_template('footer_cronsimulator.php');
			if( $C->DEBUG_MODE ) { $this->load_template('footer_debuginfo.php'); }
		?>
		
		<?php
			@include( $C->INCPATH.'../themes/include_in_footer.php' );
		?>
	</body>
</html>